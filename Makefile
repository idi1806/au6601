TARGET_MODULE := au6601-pci

obj-m += $(TARGET_MODULE).o

$(TARGET_MODULE)-objs := au6601.o


DEFAULTKDIR := /lib/modules/$(shell uname -r)/build
KDIR ?= $(DEFAULTKDIR)
PWD := $(shell pwd)

all:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean

